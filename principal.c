/* Universidade de Brasília - Faculdade do Gama
 * Estruturas de Dados 1 - 193704 TURMA: B
 * Professor: Mateus Mendelson
 * Nomes: Bruno Carmo Nunes 18/0117548
 *        Guilherme Dourado 16/0123020
 *        Guilherme Peixoto 17/0034941
 * Trabalho 3
*/

#define MAX_SIZE_NOME 101
#define MAX_SIZE_ENDERECO 105
#define MAX_SIZE_TELEFONE 12
#define MAX_SIZE_DATA 10
#define MAX_SIZE_CEP 8
#include "main.h"
#include "fileManipulation.h"
#include <string.h>

void menu_escolha(char *);
int main(){
	char opcao_id[10];
    
    printf("Vou imprimir a lista!\n");
    imprime(loadListFromFile());
    
		// do{
		// 	printf("*---------------------------------------------------------------------------*\n");
		// 	printf("Olá, bem vindo a sua Agenda de Contatos\n");
		// 	printf("\tSuas opções são: \n");
		// 	printf("\t1) Inserir novo Contato:\n\t2) Remover contato: \n\t3) Vizualizar contatos por parte do nome:\n\t4) Vizualizar todos os contatos:\n\t0) Sair.\n");
		// 	printf("*---------------------------------------------------------------------------*\n");

		// 	printf("Digite o número do menu desejado com apenas 1 dígito: \n");

		// 	scanf(" %s",opcao_id);
		// 	menu_escolha(opcao_id);

		// }while(opcao_id[0] != '0');

	return 0;

}

void menu_escolha(char *id){
		ListaAgenda *contatos;
		FILE *arquivo_contatos;
		arquivo_contatos = readFile();
		contatos = loadListFromFile();
		
		if(id[0] == '1' && id[1] == '\0'){
			//contatos = insere(contatos, arquivo_contatos);
			// loadListFromFile();
			system("clear");
		}
		else if(id[0] == '2' && id[1] == '\0'){
			eliminar(contatos);
			system("clear");
		}
		else if(id[0] == '3' && id[1] == '\0'){
			procurar(contatos);
			system("clear");
		}
		else if(id[0] == '4' && id[1] == '\0'){
			imprime(contatos);
			system("clear");
		}
		else{
			system("clear");
			printf("\t\t\tDIGITE UMA OPÇÃO VÁLIDA!!!\n\n\n");
		}


}

ListaAgenda *criar(){
    return NULL;
}

ListaAgenda *insere(ListaAgenda *l, FILE *arquivo_contatos) {
    ListaAgenda *novo = (ListaAgenda *) malloc (sizeof(ListaAgenda));
	if(novo != NULL){
		char sCEP[MAX_SIZE_CEP];
		char *ptr;

		printf("Digite o nome: ");       
		fgets(novo->Nome, MAX_SIZE_NOME, arquivo_contatos);

		printf("Digite o telefone: ");           
		fgets(novo->Telefone, MAX_SIZE_TELEFONE, arquivo_contatos);

		printf("Digite o endereço: ");            
		fgets(novo->Endereco, MAX_SIZE_ENDERECO, arquivo_contatos);

		printf("Digite o CEP: ");                 
		fgets(sCEP, MAX_SIZE_CEP, arquivo_contatos);
		
		// novo->CEP = strtol(sCEP, &ptr, 10);
		printf("Digite a data(dd/mm/aaaa): ");  
		fgets(novo->Data, MAX_SIZE_DATA, arquivo_contatos);
		fprintf(arquivo_contatos,novo->marcadorFinal);
		putchar('\n');

		novo->prox = l;
		novo->ante = NULL;

    if (l != NULL)
        l->ante = novo;
	}
    
    return novo;
}

ListaAgenda *exclui(ListaAgenda *l,char *a){

    ListaAgenda *p = buscar(l,a);

    if (p == NULL)
        return l;

    //Fazer uma condição onde só para o primeiro da lista

    if(p == l)
        l = p->prox;
    else
        p->ante->prox = p->prox;

    if(p->prox != NULL)
        p->prox->ante = p->ante;

    free(p);

    return l;

}

ListaAgenda *buscar(ListaAgenda* l, char *a) {
    ListaAgenda *p;

    for (p=l;p!=NULL;p=p->prox)
        if (strpbrk(p->Nome, a) != NULL)
            return p;
    return NULL;
}

void procurar(ListaAgenda *l){

    char a[MAX_SIZE_NOME];

    printf("Digite um nome para buscar: "); fgets(a, MAX_SIZE_NOME, stdin);
    printf("---------------------------------------------\n");
    if(buscar(l, a) != NULL) {
        imprime(buscar(l, a));
    }
    else printf("Nome não encontrado\n");


}

void imprime(ListaAgenda *l) {
    printf("Entrei na imprime!\n");
    ListaAgenda *lista;


    for(lista = l; lista != NULL; lista = lista->prox) {
        printf("Nome: %s", lista->Nome);
        printf("Telefone: %s", lista->Telefone);
        printf("Endereço: %s", lista->Endereco);
        printf("CEP: %d\n", lista->CEP);
        printf("Data: %s", lista->Data);

        printf("---------------------------------------------\n");
        fflush(stdin);

    }
}

void eliminar(ListaAgenda *l) {
    char a[MAX_SIZE_NOME];

    printf("Digite um nome para excluir: "); fgets(a, MAX_SIZE_NOME, stdin);
    printf("---------------------------------------------\n");
    exclui(l,a);

}

void liberar(ListaAgenda *l) {

    ListaAgenda *lista;

    for(lista = l; lista != NULL; l = lista) {
        lista = lista->prox;
        free(l);
    }

}