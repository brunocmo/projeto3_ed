/* Universidade de Brasília - Faculdade do Gama
 * Estruturas de Dados 1 - 193704 TURMA: B
 * Professor: Mateus Mendelson
 * Nomes: Bruno Carmo Nunes 18/0117548
 *        Guilherme Dourado 16/0123020
 *        Guilherme Peixoto 17/0034941
 * Trabalho 3
*/

#ifndef MAIN_H
#define MAIN_H

#define MAX_SIZE_NOME 102
#define MAX_SIZE_ENDERECO 106
#define MAX_SIZE_TELEFONE 13
#define MAX_SIZE_DATA 12

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
struct agenda {
    char Nome[MAX_SIZE_NOME];
    char Telefone[MAX_SIZE_TELEFONE]; // 'xxxxx-xxxx'
    char Endereco[MAX_SIZE_ENDERECO];
    char CEP[MAX_SIZE_CEP];
    char Data[MAX_SIZE_DATA]; // 00/00/0001
    char marcadorFinal[MAX_SIZE_CEP];
    struct agenda *prox;  //aponta para o valor proximo da lista
    struct agenda *ante;  //aponta para o valor anterior da lista
};
typedef struct agenda ListaAgenda;

ListaAgenda *criar();
ListaAgenda *insere(ListaAgenda *l, FILE *);
ListaAgenda *buscar(ListaAgenda* l, char *a);
ListaAgenda *exclui(ListaAgenda *l,char *a);
void procurar(ListaAgenda *l);
void imprime(ListaAgenda *l);
void eliminar(ListaAgenda *l);
void liberar(ListaAgenda *l);
ListaAgenda *SORT(ListaAgenda *nodes);

#endif