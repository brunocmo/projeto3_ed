#ifndef fileManipulation_H
#define fileManipulation_H
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "SORT.h"



FILE *readFile();
void generateFile();
void updateFile(ListaAgenda *);
ListaAgenda *loadListFromFile();

FILE *readFile(){
    FILE *contatos_arquivo;
    contatos_arquivo = fopen("contatos.txt", "r");
    if(contatos_arquivo != NULL){
        printf("Agenda aberta com sucesso!\n");
    }
    else{
        contatos_arquivo = NULL;
    }

    return contatos_arquivo;
}

void generateFile(){
    FILE *arquivo_gerado;
    arquivo_gerado = fopen("contatos.txt", "w");
    fclose(arquivo_gerado);
}

void updateFile(ListaAgenda *l){
    FILE *agenda_file;
    agenda_file = fopen("contatos.txt", "a");
    if(agenda_file != NULL){
        ListaAgenda *lista;
        for(lista = l; lista != NULL; lista = lista->prox) {
            fprintf(agenda_file, "%s", lista->Nome);
            fprintf(agenda_file, "%s", lista->Telefone);
            fprintf(agenda_file, "%s", lista->Endereco);
            fprintf(agenda_file, "%s\n", lista->CEP);
            fprintf(agenda_file, "%s", lista->Data);
            fprintf(agenda_file, "$");
        }
    }
    fclose(agenda_file);
}

ListaAgenda *loadListFromFile(){
    printf("Vou carregar a lista\n");
    FILE *agenda_file;
    agenda_file = readFile();
    ListaAgenda *list, *atual, *node;
    node = atual = NULL;
    if(agenda_file != NULL){ 
        rewind(agenda_file);
        while (!feof(agenda_file)){ 
            list =  malloc(sizeof(ListaAgenda));
            fgets(list->Nome, sizeof(list->Nome), agenda_file);
            fgets(list->Telefone, sizeof(list->Telefone), agenda_file);
            fgets(list->Endereco, sizeof(list->Endereco), agenda_file);
            fgets(list->CEP, sizeof(list->CEP), agenda_file);
            fgets(list->Data, sizeof(list->Data), agenda_file);
            fgets(list->marcadorFinal, sizeof(list->marcadorFinal), agenda_file);
            list->prox = NULL;
            
            if(node == NULL){
                atual = node = list;
            }
            else{
                atual = atual->prox = list;
            }
            
            // printf("Actually reading it..\n");
        }

        fclose(agenda_file);
        // for(atual = node; atual; atual = atual->prox){
        //     printf("Nome:%s", atual->Nome);
        //     printf("Telefone:%s", atual->Telefone);
        //     printf("Endereço:%s", atual->Endereco);
        //     printf("CEP:%s", atual->CEP);
        //     printf("Data:%s", atual->Data);
        // }
        ListaAgenda *return_list;
        return_list = sortList(node);
        return return_list;
    }
    else{
        printf("Não há nenhum contato em sua Agenda!\n");
        list = NULL;
    }
    fclose(agenda_file);
}

#endif
